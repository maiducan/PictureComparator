#include "surflib.h" // --- OpenSURF --- This library is distributed under the GNU GPL.
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <pthread.h>
/* c++ 11 */
#include <thread>
#include <mutex>

using namespace std;


//-----------------------------------------------------------------------------------------------------------//
/* Global variables */
vector<vector<int> >                                    m_Similarities; // 2D matrix of similarities
//-----------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------//
/* Structure with data for multi-threading calculation */
struct TSurf {

    string                                              m_Img1, m_Img2, m_Predicate;
    int                                                 m_I, m_J, m_PredicateValue;

};
//-----------------------------------------------------------------------------------------------------------//
/* Sorting algorithm for knn calculation */
struct sort_pred {
    bool operator()(const std::pair<int,int> &left, const std::pair<int,int> &right) {
        return left.second > right.second;
    }
};
//-----------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------//
/* Thread function */
void * OpenSurf (void * a) {

    /* Getting data from structure */
    TSurf * cmp = (TSurf *) a;
    int predicate_value = cmp -> m_PredicateValue;
    int i = cmp -> m_I;
    int j = cmp -> m_J;

    /* Loading images */
    IplImage *img1, *img2;
    img1 = cvLoadImage((cmp -> m_Img1).c_str());
    img2 = cvLoadImage((cmp -> m_Img2).c_str());

    /* Getting interesting points from images */
    IpVec ipts1, ipts2;
    surfDetDes(img1,ipts1,true,4,2,2,0.001f);
    surfDetDes(img2,ipts2,true,4,2,2,0.001f);

    /* Comparing pictures and getting similarity value */
    IpPairVec matches;
    if (cmp -> m_Predicate == "range") getMatches(ipts1,ipts2,matches, 0, predicate_value);
    else if (cmp -> m_Predicate == "knn") getMatches(ipts1,ipts2,matches, 1, predicate_value);

    /* Saving results into matrix of images */
    m_Similarities[i][j] = matches.size();

    /* Debugging stuffs */
//    cvNamedWindow("1", CV_WINDOW_AUTOSIZE );
//    cvNamedWindow("2", CV_WINDOW_AUTOSIZE );
//    cvShowImage("1", img1);
//    cvShowImage("2",img2);
//
//    cvWaitKey(0);

    return (NULL);
}
//-----------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------//
class CImage {

public:

                                                        CImage (string, string, string);
                                                       ~CImage ();

    bool                                                controlFiles (string, string, string);
    void                                                getNamesOfImages (int);
    void                                                getPredicate ();

    void                                                resizeMatrix ();

    void                                                Compare ();

    void                                                getSimilarityValues ();
    void                                                rangePredicate ();
    void                                                knnPredicate ();


private:

    ifstream                                            m_File1, m_File2, m_FilePredicate;
    ofstream                                            m_Output;

    vector<string>                                      m_ImageNames1;      // list of images from file1
    vector<string>                                      m_ImageNames2;      // list of images from file2
    vector<string>::iterator                            it1, it2;

    string                                              m_Predicate;
    int                                                 m_PredicateValue;

    int                                                 m_Num1;             // number of images from file1
    int                                                 m_Num2;             // num file 2

    bool                                                m_Identity;

    /* Thread declaration */

    int                                                 m_ThreadsCnt;
    pthread_t                                        *  m_ThrID;
    pthread_attr_t                                      m_Attr;

};
//-----------------------------------------------------------------------------------------------------------//
CImage::CImage(string file1, string file2, string filePredicate) {

    m_PredicateValue = 0;
    m_Num1 = 0;
    m_Num2 = 0;
    m_Identity = false;

    m_File1.open(file1.c_str());
    m_File2.open(file2.c_str());
    m_FilePredicate.open(filePredicate.c_str());

    /* Thread inicialization */

    // m_ThrID is inicialized later, after the knoledge of number of images

    m_ThreadsCnt = 0;
    pthread_attr_init ( &m_Attr );
    pthread_attr_setdetachstate ( &m_Attr, PTHREAD_CREATE_JOINABLE );

    /* Control validity of files */
    if(!controlFiles(file1, file2, filePredicate)) cout << "Please make sure, that files are good! Bye." << endl;


}
//-----------------------------------------------------------------------------------------------------------//
CImage::~CImage() {

    m_File1.close();
    m_File2.close();
    m_FilePredicate.close();

    pthread_attr_destroy ( &m_Attr );
    delete [] m_ThrID;

}
//-----------------------------------------------------------------------------------------------------------//
bool CImage::controlFiles(string f1, string f2, string f3) {

    if (!m_File1.good()) {
        cout << f1 << " is not valid!" << endl;
        return false;
    }
    else getNamesOfImages(1);


    if (!m_File2.good()) {
        cout << f2 << " is not valid!" << endl;
        return false;
    }
    else getNamesOfImages(2);


    if (!m_FilePredicate.good()) {
        cout << f3 << " is not valid!" << endl;
        return false;
    }
    else getPredicate();

    return true;
}
//-----------------------------------------------------------------------------------------------------------//
void CImage::getNamesOfImages(int type) {

    string line;
    string pathFileName = "img/uploaded_images/";         // different directory in view of web

    /* Parse File 1 */
    if (type == 1) {
        while (getline(m_File1, line)) {

            m_ImageNames1.push_back(pathFileName+"file1/"+line);
            m_Num1++;

        }
    }

    /* Parse File 2 */
    if (type == 2) {
        while (getline(m_File2, line)) {

            m_ImageNames2.push_back(pathFileName+"file2/"+line);
            m_Num2++;

        }
    }

}
//-----------------------------------------------------------------------------------------------------------//
void CImage::getPredicate() {

    string line;
    int cnt = 0;

    while (getline(m_FilePredicate, line)) {

        switch(cnt) {
            case 0: m_Predicate = line; break;
            case 1: m_PredicateValue = atoi(line.c_str()); break;
        }

        cnt++;
    }

}
//-----------------------------------------------------------------------------------------------------------//
void CImage::resizeMatrix () {

    m_Similarities.resize(m_Num1+1);            // for "human" counting from image 1, not indexing from zero

        for (int i = 0; i < m_Num1+1; i++) {

            m_Similarities[i].resize(m_Num2+1);     // +1 for getting max similarity value of image itself

    }
}
//-----------------------------------------------------------------------------------------------------------//
void CImage::Compare() {

    /* Compare each image from file1 with each image from file 2, finally gets their similarity value */

    /* One thread for each comparison (one on one) */
    m_ThrID = new pthread_t[(m_Num1+1)*m_Num2]; // m_Num1 + 1 for thread, which compares image with it self

    int i = 1, j = 1;

    for (it1 = m_ImageNames1.begin(); it1 != m_ImageNames1.end(); ++it1) {

        TSurf * cmp = new TSurf();
        cmp -> m_Predicate = m_Predicate;
        cmp -> m_PredicateValue = m_PredicateValue;
        cmp -> m_I = i;
        cmp -> m_J = 0;
        cmp -> m_Img1 = *it1;
        cmp -> m_Img2 = *it1;

        pthread_create(&m_ThrID[m_ThreadsCnt], &m_Attr, OpenSurf, (void*) cmp);
        m_ThreadsCnt++;

        for (it2 = m_ImageNames2.begin(); it2 != m_ImageNames2.end(); ++it2) {

            TSurf * cmp = new TSurf();
            cmp -> m_Predicate = m_Predicate;
            cmp -> m_PredicateValue = m_PredicateValue;
            cmp -> m_I = i;
            cmp -> m_J = j;
            cmp -> m_Img1 = *it1;
            cmp -> m_Img2 = *it2;

            pthread_create(&m_ThrID[m_ThreadsCnt], &m_Attr, OpenSurf, (void*) cmp);
            m_ThreadsCnt++;

            j++;
        }
        j = 1;
        i++;
    }

    for (int k = 0; k < m_ThreadsCnt; k++)
        pthread_join (m_ThrID[k], NULL );


}
//-----------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------//
/* Saving results in the output file */
void CImage::getSimilarityValues() {

    m_Output.open("src/similarity.txt");             // diff dir in web

    if (m_Output.good()) {

        // Print Range
        if (m_Predicate == "range") rangePredicate();
        // Print kNN
        else if (m_Predicate == "knn") knnPredicate();

    }

}
//-----------------------------------------------------------------------------------------------------------//
/* Algorithm for range calculation */
void CImage::rangePredicate() {

    m_Output << "range" << endl;

    for (int i = 1; i < m_Num1+1; i++) {

        m_Output << "#" << i << ".jpg" << endl;
        double ratio = m_Similarities[i][0] * (double)m_PredicateValue/100;

        for (int j = 1; j < m_Num2+1; j++) {

            if (m_Similarities[i][j] >= ratio) {

                m_Output << j << ".jpg:" << setprecision(3) << (double)m_Similarities[i][j]/m_Similarities[i][0]*100 << endl;

            }
        }
    }
}
//-----------------------------------------------------------------------------------------------------------//
/* Algorithm for k-nearest-neighbours */
void CImage::knnPredicate() {

    m_Output << "knn" << endl;

    /* compared image with its similarity value */
    vector<pair<int, int> > tmp;

    for (int i = 1; i < m_Num1+1; i++) {

        m_Output << "#" << i << ".jpg" << endl;

        for (int j = 1; j < m_Num2+1; j++) {

            tmp.push_back(make_pair (j, m_Similarities[i][j]));

        }

        std::sort(tmp.begin(), tmp.end(), sort_pred());

        int numImagesToShow = 0;

        if (m_PredicateValue <= (signed)tmp.size()) numImagesToShow = m_PredicateValue;
        else numImagesToShow = tmp.size();

        for (int k = 0; k < numImagesToShow; k++) m_Output <<  tmp[k].first << ".jpg" << endl;

        tmp.clear();

    }

}
//-----------------------------------------------------------------------------------------------------------//


int main(void) {

    /* Open files (images + predicate), control, read them and save images */
    CImage * inventory = new CImage("src/images1.txt", "src/images2.txt", "src/predicate.txt");                // different dir in view of web

    /* Inicialize matrix for storing values */
    inventory -> resizeMatrix();

    /* Compare 2 vectors (collection) of images */
    inventory -> Compare();

    /* Output file with datas */
    inventory -> getSimilarityValues();

    delete inventory;

}
