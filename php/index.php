<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Similarity Join - Picture Compare - VWM Projekt</title>
        <link rel="stylesheet" type="text/css" href="css/layout.css"/>
        
        <script src="js/jquery-1.11.2.js"></script>
        <script src="http://malsup.github.io/jquery.blockUI.js"></script>
        <script src="js/ducan_scripts.js"></script>

        
    </head>
    <body>
        
        <?php
            if (isset($_GET['delete'])) {
                if ($_GET['delete'] == 1) {
                    include 'src/clear.php';
                }
            }
       
            
            ini_set('upload_max_filesize', '100M');
            ini_set('max_file_uploads', '100');
            
        ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

         <header>
            <h1>Similarity join | Porovnávání obrázků | BI-VWM</h1>
        </header>
        
<!--------------------------------------------------------------------------------------------------------------------------------->
        
        <div id="form_zone">
            
            <form action ="result.php" method="post" enctype="multipart/form-data">
                
                <div id="upfile_zone">
                
                    <h2>Kolekce obrázků:</h2>
                        
                    <br />
                    1. kolekce obrázků: <input type="file" name="files1[]" multiple required /> 
                    <br /> <br />
                    2. kolekce obrázků: <input type="file" name="files2[]" multiple required /> 
                
                </div>
                
<!--------------------------------------------------------------------------------------------------------------------------------->
                
            <div id="predicate_zone">   
                    <h2>Způsob porovnání (predikát):</h2>
                    <br/>

                    Rozsahový <input type="radio" name="predicate" value="range" checked="checked"/>
                    Rozsah (%): <input type="number" id="predicateValueRange" name="rangeValue" max="100" min="1" placeholder="Podobnostní vzdálenost" required />
                    
                    <br/> <br />
                    
                    kNN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="predicate" value="knn" />
                    kNN (1-10): <input type="number" id="predicateValueKnn" name="knnValue" max="10" min="1" placeholder="Počet nejpodobnějších obrázků" disabled required />

                    <br /><br />
                    
            </div>
                
<!--------------------------------------------------------------------------------------------------------------------------------->
                
                <input type="submit" id="loading" name="submit" value="Nahrát a porovnat" />


            </form>
            
            
  
        </div>
        
<!--------------------------------------------------------------------------------------------------------------------------------->
        
    </body>
</html>
