<?php

if(isset($_REQUEST["submit"])) {
 
    
    /* WRITE NAME OF IMAGES INTO FILE */
//--------------------------------------------------------------------------------------------------------------------//    
    $textFile1 = "src/images1.txt";
    $textFile2 = "src/images2.txt";
    

    for($i=0; $i<count($_FILES['files1']['name']); $i++) {    // indexing images from 1 like human

        $FileName = $_FILES['files1']['name'][$i];
        $tmpFilePath = $_FILES['files1']['tmp_name'][$i];

        $FilePath = "./img/uploaded_images/file1/" . ($i+1) . ".jpg";

        if(move_uploaded_file($tmpFilePath, $FilePath)) {
            //echo '<p class="upload_echo">1. kolekce, úspěšně nahrán: ' . $FileName . '</p>';
            
            file_put_contents($textFile1, ($i+1).".jpg\n", FILE_APPEND | LOCK_EX);
            
        }
        //else echo '<p class="upload_echo">1. kolekce, obrázek ' . $FileName . ' se nenahrál.</p>';
    
    }
    
    for($i=0; $i<count($_FILES['files2']['name']); $i++) {    // indexing images from 1 like human

    $FileName = $_FILES['files2']['name'][$i];
    $tmpFilePath = $_FILES['files2']['tmp_name'][$i];

    $FilePath = "./img/uploaded_images/file2/" . ($i+1) . ".jpg";

    if(move_uploaded_file($tmpFilePath, $FilePath)) {
        //echo '<p class="upload_echo">2. kolekce, úspěšně nahrán: ' . $FileName . '</p>';
     
        file_put_contents($textFile2, ($i+1).".jpg\n", FILE_APPEND | LOCK_EX);
        
    }
    //else echo '<p class="upload_echo">2. kolekce, obrázek ' . $FileName . ' se nenahrál.</p>';
    
    }
//--------------------------------------------------------------------------------------------------------------------//
  
    /* PREDICATE [range or kNN] */
//--------------------------------------------------------------------------------------------------------------------//
    
    if (isset($_REQUEST['predicate'])) {
        
        $predicate = $_POST['predicate'];
        $predicateFile = "src/predicate.txt";
        
        switch ($predicate):
            case 'range': {
                
                echo '<h2>Podobnostní predikát</h2>';
                
                if (isset($_REQUEST['rangeValue'])) {
                    
                    $rangeValue = $_REQUEST['rangeValue'];  
                    
                    echo "<h3>Hodnota: alespoň z " . $rangeValue . "% podobné</h3><br/>";
                    
                    file_put_contents($predicateFile, "range\n" . $rangeValue);
                    
                }
                
                break;
                
            }
                
            case 'knn': {
                
                echo '<h2>K-nejbližších sousedů</h2>';
                
                if (isset($_REQUEST['knnValue'])) {
                    
                    $knnValue = $_REQUEST['knnValue'];                    
                    echo '<h3>Hodnota: ' . $knnValue . " nejpodobnějších obrázků</h3><br/>";
                    
                    file_put_contents($predicateFile, "knn\n" . $knnValue);
                    
                    
                }
                
                
                break;
                
            }
         
        endswitch;
        
    }
    
//--------------------------------------------------------------------------------------------------------------------//
    
}




