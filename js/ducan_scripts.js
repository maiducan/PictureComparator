/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Loading process */
function loading() {
    
    $('#loading').click(function() { 
        $.blockUI({

            message: 'Probíhá porovnávání obrázků',
            css: {

                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff' 
            }
        }); 
            setTimeout($.unblockUI, 50000); 
    }); 
    
    
}

/* Show and hide / slide result images */
function showResults() {
    
    $('#result_zone').fadeIn(1500);
    
}

function toggleEachResults(){
    
    
    
    
}

function disableNumberValue() {
    
    $('input[type=radio]').each(function(i){
        $(this).click(function () {
            if(i==1) { //2nd radiobutton
                    $("#predicateValueRange").attr("disabled", "disabled");
                    $("#predicateValueKnn").removeAttr("disabled"); 
            }
            else {
                    $("#predicateValueKnn").attr("disabled", "disabled");
                    $("#predicateValueRange").removeAttr("disabled"); 
            }
        });

    });    
    
    
}

$(document).ready(function() {

    /* index.php */
    loading();
    disableNumberValue();

    /* result.php */
    showResults();
    toggleEachResults();
     
    
});